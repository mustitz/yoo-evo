#define INNER_JOIN_IDS(a, b) a ## _ ## b
#define JOIN_IDS(a, b) INNER_JOIN_IDS(a, b)

#ifdef YOO_EVO_PREFIX
#define YOO_ID(name) JOIN_IDS(YOO_EVO_PREFIX, name)
#else
#define YOO_ID(name) name
#endif

void YOO_ID(EA_1_plus_1)(struct habitat * restrict const habitat)
{
    struct individ * restrict best = create(habitat);
    determine_fitness(habitat, best);

    while (!basta(habitat, best))  {
        struct individ * child = mutate(habitat, best);
        determine_fitness(habitat, child);
        if (is_not_worse(child, best)) {
            kill(habitat, best);
            best = child;
        }
    }
}

#ifdef YOO_EVO_PREFIX
#undef YOO_EVO_PREFIX
#endif

#ifdef determine_fitness
#undef determine_fitness
#endif

#ifndef mutate
#undef mutate
#endif
