#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef uint64_t bits_t;
typedef unsigned int fitness_t;

#define BITS_ONE  ((bits_t)1)
#define BITS_ONES (~(bits_t)0)

struct individ
{
    fitness_t fitness;
    bits_t bits[1];
};

struct entropy
{
    const unsigned int * begin;
    const unsigned int * ptr;
    const unsigned int * end;
};

struct habitat
{
    unsigned int N;
    unsigned int p_limit;
    size_t bits_sz;
    bits_t last_mask;
    struct individ * population[2];
    struct individ * embrione;
    struct entropy * entropy;
    unsigned int qiterations;
    unsigned int (*random)(struct habitat * restrict const me);
};

typedef void (*evo_alg_f)(struct habitat * restrict const);


static unsigned int sys_random(struct habitat * restrict const me)
{
    return rand();
}

static unsigned int entropy_random(struct habitat * restrict const me)
{
    struct entropy * restrict const entropy = me->entropy;
    const unsigned int value = *entropy->ptr++;
    if (entropy->ptr == entropy->end) {
        entropy->ptr = entropy->begin;
    }
    return value;
}


static inline struct individ * std_mutate(
    struct habitat * restrict const me,
    const struct individ * const parent)
{
    struct individ * restrict const newborn = me->embrione;
    memcpy(newborn->bits, parent->bits, me->bits_sz);

    bits_t * restrict ptr = newborn->bits;
    bits_t mask = 1;
    for (unsigned int i=0; i<me->N; ++i) {
        unsigned int rnd = me->random(me);
        if (rnd <= me->p_limit) {
            *ptr ^= mask;
        }

        mask <<= 1;
        if (mask == 0) {
            mask = 1;
            ++ptr;
        }
    }

    return newborn;
}


static inline struct individ * create(struct habitat * restrict const me)
{
    me->embrione = me->population[1];
    struct individ * restrict const newborn = me->population[0];

    const size_t qbits = me->bits_sz / sizeof(bits_t);
    for (int i=0; i<qbits; ++i) {
        bits_t value = 0;
        for (int j=0; j<sizeof(bits_t)/2; ++j) {
            value <<= 16;
            value |= me->random(me) & 0xFFFF;
        }
        newborn->bits[i] = value;
    }
    newborn->bits[qbits-1] &= me->last_mask;
    return newborn;
}

static inline void kill(
    struct habitat * restrict const me,
    struct individ * restrict const individ)
{
    me->embrione = individ;
}


static inline int is_not_worse(
    const struct individ * const a,
    const struct individ * const b)
{
    return a->fitness <= b->fitness;
}


static inline int basta(
    struct habitat * restrict const me,
    const struct individ * restrict const best)
{
    const int in_progress = best->fitness != 0;
    me->qiterations += in_progress;
    return !in_progress;
}


static inline fitness_t calc_needle(
    struct habitat * restrict const me,
    struct individ * restrict const individ)
{
    const bits_t * ptr = individ->bits;
    const bits_t * const end = ptr + me->bits_sz / sizeof(bits_t);
    for (;;) {
        if (ptr == end-1) {
            return *ptr != me->last_mask;
        }
        if (*ptr++ != BITS_ONES) {
            return 1;
        }
    }
    return 0;
}

static inline void needle(
    struct habitat * restrict const me,
    struct individ * restrict const individ)
{
    individ->fitness = calc_needle(me, individ);
}


static inline fitness_t calc_onemax(
    struct habitat * restrict const me,
    struct individ * restrict const individ)
{
    const bits_t * ptr = individ->bits;
    const bits_t * const end = ptr + me->bits_sz / sizeof(bits_t);
    fitness_t sum = 0;
    for (; ptr != end; ++ptr) {
        sum += __builtin_popcountll(*ptr);
    }
    return me->N - sum;
}

static inline void onemax(
    struct habitat * restrict const me,
    struct individ * restrict const individ)
{
    individ->fitness = calc_onemax(me, individ);
}


#define YOO_EVO_PREFIX needle_rls
#define determine_fitness needle
#define mutate std_mutate
#include "evo-algs.c"

#define YOO_EVO_PREFIX onemax_rls
#define determine_fitness onemax
#define mutate std_mutate
#include "evo-algs.c"

int run(
    const unsigned int N,
    evo_alg_f evo_alg,
    const char * const entropy_file,
    unsigned int * restrict const qiterations)
{
    const size_t offset = offsetof(struct individ, bits);
    const size_t qbits = 8 * sizeof(bits_t);
    const unsigned int mod = N % qbits;
    const size_t bits_len = (N / qbits) + !!(N % qbits);
    const size_t bits_sz = bits_len * sizeof(bits_t);
    const size_t sz = offsetof(struct individ, bits) + bits_sz;

    struct habitat habitat;
    struct habitat * restrict const me = &habitat;

    const unsigned int max_rand = entropy_file ? ~(unsigned int)0 : RAND_MAX;

    char * const data = malloc(2 * sz);
    if (data == NULL) {
        return ENOMEM;
    }

    me->N = N;
    me->p_limit = max_rand / N;
    me->bits_sz = bits_sz;
    me->last_mask = mod == 0 ? BITS_ONES : (BITS_ONE << mod) - 1;
    me->population[0] = (void*)(data + 0*sz);
    me->population[1] = (void*)(data + 1*sz);
    me->embrione = NULL;
    me->entropy = NULL;
    me->qiterations = 0;

    me->random = entropy_file ? entropy_random : sys_random;

    evo_alg(me);
    if (qiterations != NULL) {
        *qiterations = me->qiterations;
    }

    free(data);
    return 0;
}


#include <getopt.h>
#include <stdio.h>
#include <time.h>

static struct option long_options[] = {
    { "help", no_argument, NULL, 'h' },
    { "seed", required_argument, NULL, 'S' },
    { NULL, 0, NULL, 0 }
};

static const char * help[] = {
    "Show help message",
    "Seed for random generator",
};

static void print_usage(const char * const app)
{
    printf("Usage: %s [OPTION]... N fintess\n", app);
    for (int i=0;; ++i) {
        const char * name = long_options[i].name;
        if (name == NULL) {
            break;
        }
        printf("  --%-12s %s\n", name, help[i]);
    }
}

static int get_N(const char * const str, unsigned int * restrict const value)
{
    if (str == NULL || *str == '\0') {
        fprintf(stderr, "Invalid empty value for N\n");
        return EINVAL;
    }

    char * endptr;
    *value = strtoul(str, &endptr, 10);
    if (*endptr != '\0') {
        fprintf(stderr, "Invalid value “%s” for an positive integer constant N\n", str);
        return EINVAL;
    }

    if (*value == 0) {
        fprintf(stderr, "Invalid value “%s” N might be at least one\n", str);
        return EINVAL;
    }

    return 0;
}

static const char * fintesses[] = {
    "needle", "onemax"
};

static const char * algorithms[] = {
    "1+1EA"
};

enum { QFITNESSES = sizeof(fintesses) / sizeof(fintesses[0]) };

static evo_alg_f evo_algs[QFITNESSES] = {
    needle_rls_EA_1_plus_1, onemax_rls_EA_1_plus_1
};

static int get_fitness(
    const char * const str,
    int * restrict const index)
{
    for (int i=0; i<QFITNESSES; ++i) {
        if (strcmp(str, fintesses[i]) == 0) {
            *index = i;
            return 0;
        }
    }

    fprintf(stderr, "Unknown value “%s” for fitness function, supported names are:", str);
    const char * separator = "";
    for (int i=0; i<QFITNESSES; ++i) {
        fprintf(stderr, "%s %s", separator, fintesses[i]);
        separator = ",";
    }
    fprintf(stderr, "\n");
    return EINVAL;
}

int main(int argc, char * argv[])
{
    int status;
    enum parse_status { OK=0, ERROR=1, HELP=2 } parse_status = OK;

    for (;;) {
        int option_index = 0;
        const int c = getopt_long(argc, argv, "S:h", long_options, &option_index);
        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                fprintf(stderr, "getopt_long: setting flag\n");
                break;

            case '?':
                fprintf(stderr, "getopt_long: unknown option\n");
                parse_status = ERROR;
                break;

            case 'S':
                printf("Seed: %s\n", optarg);
                break;

            case 'h':
                if (parse_status == OK) {
                    parse_status = HELP;
                }
                break;

            default:
                fprintf(stderr, "getopt_long: unexpected value\n");
                break;
        }
    }

    if (optind + 2 < argc) {
        parse_status = ERROR;
        fprintf(stderr, "%s: too many mandatory arguments\n", argv[0]);
    }

    if (optind + 2 > argc) {
        parse_status = ERROR;
        fprintf(stderr, "%s: mandatory argumets are missed\n", argv[0]);
    }

    if (parse_status != OK) {
        print_usage(argv[0]);
        return parse_status == ERROR ? EINVAL : 0;
    }

    unsigned int N;
    int ifitness;

    status = get_N(argv[optind+0], &N);
    if (status != 0) {
        return status;
    }

    status = get_fitness(argv[optind+1], &ifitness);
    if (status != 0) {
        return status;
    }

    evo_alg_f evo_alg = evo_algs[ifitness];
    if (evo_alg == NULL) {
        fprintf(stderr, "Unsupported combination for fitness function/algorithm\n");
        return EINVAL;
    }

    printf("N: %u\n", N);
    printf("Fitness: %s\n", fintesses[ifitness]);

    srand(time(NULL));

    float sum = 0.0;
    const unsigned int ITERATIONS = 10;
    for (int i=0; i<ITERATIONS; ++i) {
        unsigned int qiterations;
        const int status = run(N, evo_alg, NULL, &qiterations);
        if (status != 0) {
            return status;
        }
        sum += qiterations;
    }

    printf("EV[T] = %.2f\n", sum / ITERATIONS);
    return 0;
}
